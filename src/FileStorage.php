<?php

namespace Gsdk\FileStorage;

use Illuminate\Support\Facades\Facade;

/**
 * @method static FileInfo|null find(string $guid)
 * @method static FileInfo create(string $name, string $contents)
 * @method static void update(string $guid, string $name, string $contents)
 * @method static void delete(string $guid)
 */
class FileStorage extends Facade
{
    protected static function getFacadeAccessor()
    {
        return FileStorageManager::class;
    }
}
