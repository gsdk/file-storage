<?php

namespace Gsdk\FileStorage\Storage;

use Gsdk\FileStorage\Contracts\CacheStorageInterface;
use Gsdk\FileStorage\Entity\File;
use Gsdk\FileStorage\ValueObject\Guid;
use Illuminate\Redis\Connections\Connection;
use Illuminate\Support\Facades\Redis;

class RedisCache implements CacheStorageInterface
{
    protected readonly Connection $connection;

    public function __construct(
        protected readonly string $table,
        string $connection,
    ) {
        $this->connection = Redis::connection($connection);
    }

    public function get(Guid $guid): ?File
    {
        $data = $this->connection->hget($this->table, $guid->value());

        return empty($data) ? null : self::unpack($data);
    }

    public function store(File $file): void
    {
        $this->connection->hset(
            $this->table,
            $file->guid()->value(),
            self::pack($file)
        );
    }

    public function forget(File $file): void
    {
        $this->connection->hdel($this->table, $file->guid()->value());
    }

    private static function pack(File $file): string
    {
        return json_encode($file->serialize());
    }

    private static function unpack(string $encoded): File
    {
        $data = json_decode($encoded, true);

        return File::deserialize($data);
    }
}
