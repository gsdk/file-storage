<?php

namespace Gsdk\FileStorage\Storage;

use Gsdk\FileStorage\Contracts\DatabaseStorageInterface;
use Gsdk\FileStorage\Entity\File;
use Gsdk\FileStorage\Exception\FileNotFoundException;
use Gsdk\FileStorage\Model\File as Model;
use Gsdk\FileStorage\ValueObject\Guid;

class DatabaseStorage implements DatabaseStorageInterface
{
    public function find(Guid $guid): ?File
    {
        $model = Model::findByGuid($guid->value());

        return $model ? self::modelToFile($model) : null;
    }

    public function create(string $name = null): File
    {
        $model = Model::create([
            'name' => $name
        ]);

        return self::modelToFile($model);
    }

    public function store(File $file): bool
    {
        $model = $this->tryFindModel($file->guid()->value());

        $model->update([
            'name' => $file->name()
        ]);

        return true;
    }

    public function delete(File $file): bool
    {
        $model = $this->tryFindModel($file->guid()->value());

        $model->delete();

        return true;
    }

    public function touch(File $file): void
    {
        $model = $this->tryFindModel($file->guid()->value());

        $model->touch();
    }

    private function tryFindModel(string $guid): Model
    {
        $model = Model::findByGuid($guid);
        if (!$model) {
            throw new FileNotFoundException('File [' . $guid . '] not found');
        }

        return $model;
    }

    private static function findExtension(?string $name): ?string
    {
        if (empty($name) || false === ($pos = strrpos($name, '.'))) {
            return null;
        }

        return substr($name, $pos);
    }

    public static function modelToFile($model): File
    {
        return new File(
            new Guid($model->guid),
            $model->name,
        );
    }
}
