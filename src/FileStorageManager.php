<?php

namespace Gsdk\FileStorage;

use Gsdk\FileStorage\Contracts\PathGeneratorInterface;
use Gsdk\FileStorage\Contracts\UrlGeneratorInterface;
use Gsdk\FileStorage\Entity\File;
use Gsdk\FileStorage\Support\FileReader;
use Gsdk\FileStorage\Support\FileUpdater;

class FileStorageManager
{
    public function __construct(
        protected readonly FileUpdater $fileUpdater,
        protected readonly FileReader $fileReader,
        protected readonly PathGeneratorInterface $pathGenerator,
        protected readonly UrlGeneratorInterface $urlGenerator,
    ) {}

    public function ignoreMigration() {}

    public function find(string $guid): ?FileInfo
    {
        return ($file = $this->fileReader->findByGuid($guid))
            ? $this->makeFileInfo($file) : null;
    }

    public function create(string $name, string $contents): FileInfo
    {
        return $this->makeFileInfo(
            $this->fileUpdater->create($name, $contents)
        );
    }

    public function update(string $guid, string $name, string $contents): FileInfo
    {
        return $this->makeFileInfo(
            $this->fileUpdater->update($guid, $name, $contents)
        );
    }

    public function delete(string $guid): void
    {
        $this->fileUpdater->remove($guid);
    }

    private function makeFileInfo(File $file): FileInfo
    {
        return new FileInfo(
            $file->guid()->value(),
            $this->urlGenerator->url($file),
            $this->pathGenerator->path($file)
        );
    }
}
