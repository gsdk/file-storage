<?php

namespace Gsdk\FileStorage\Contracts;

use Gsdk\FileStorage\Entity\File;
use Gsdk\FileStorage\ValueObject\Guid;

interface DatabaseStorageInterface
{
    public function find(Guid $guid): ?File;

    public function create(string $name = null): File;

    public function store(File $file): bool;

    public function delete(File $file): bool;

    public function touch(File $file): void;
}
