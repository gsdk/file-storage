<?php

namespace Gsdk\FileStorage\Contracts;

use Gsdk\FileStorage\Entity\File;
use Gsdk\FileStorage\ValueObject\Guid;

interface CacheStorageInterface
{
    public function get(Guid $guid): ?File;

    public function store(File $file): void;

    public function forget(File $file): void;
}
