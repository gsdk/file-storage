<?php

namespace Gsdk\FileStorage\Contracts;

use Gsdk\FileStorage\Entity\File;

interface FilesystemsStorageInterface
{
    public function get(File $file, int $part = null): ?string;

    public function put(File $file, string $contents): bool;

    public function delete(File $file): void;
}
