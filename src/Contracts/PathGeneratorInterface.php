<?php

namespace Gsdk\FileStorage\Contracts;

use Gsdk\FileStorage\Entity\File;

interface PathGeneratorInterface
{
    public function relativePath(File $file): string;

    public function path(File $file, int $part = null): string;
}
