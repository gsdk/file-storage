<?php

namespace Gsdk\FileStorage\Contracts;

use Gsdk\FileStorage\Entity\File;

interface UrlGeneratorInterface
{
    public function url(File $file, int $part = null): ?string;
}
