<?php

namespace Gsdk\FileStorage;

class FileInfo extends \SplFileInfo
{
    public function __construct(
        private readonly string $guid,
        private readonly string $url,
        string $filename
    ) {
        parent::__construct($filename);
    }

    public function getGuid(): string
    {
        return $this->guid;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getMimeType(): string
    {
        return finfo_file(finfo_open(FILEINFO_MIME_TYPE), $this->getPathname());
    }
}