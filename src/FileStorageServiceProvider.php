<?php

namespace Gsdk\FileStorage;

use Gsdk\FileStorage\Contracts\CacheStorageInterface;
use Gsdk\FileStorage\Contracts\DatabaseStorageInterface;
use Gsdk\FileStorage\Contracts\FilesystemsStorageInterface;
use Gsdk\FileStorage\Contracts\PathGeneratorInterface;
use Gsdk\FileStorage\Contracts\UrlGeneratorInterface;
use Gsdk\FileStorage\Service\FilesystemsStorage;
use Gsdk\FileStorage\Service\PathGenerator;
use Gsdk\FileStorage\Service\UrlGenerator;
use Gsdk\FileStorage\Storage\DatabaseStorage;
use Gsdk\FileStorage\Storage\RedisCache;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\ServiceProvider;

class FileStorageServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        if ($this->app->runningInConsole()) {
            $this->registerMigrations();
        }

        $this->registerServices();
    }

    public function register(): void
    {
        if (!app()->configurationIsCached()) {
            $this->mergeConfigFrom(__DIR__ . '/../config/file-storage.php', 'file-storage');
        }
    }

    protected function registerMigrations(): void
    {
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
    }

    protected function registerServices(): void
    {
        $this->app->singleton(PathGeneratorInterface::class, function () {
            $config = config('file-storage.filesystems');

            return new PathGenerator(
                Storage::disk($config['disk'])->path(''),
                $config['nesting_level'],
                $config['path_name_length'],
            );
        });

        $this->app->singleton(UrlGeneratorInterface::class, function ($app) {
            return new UrlGenerator(
                config('file-storage.filesystems.disk'),
                $app->get(PathGeneratorInterface::class)
            );
        });

        $this->app->singleton(FilesystemsStorageInterface::class, function ($app) {
            return new FilesystemsStorage(config('file-storage.filesystems'), $app->get(PathGeneratorInterface::class));
        });

        $this->app->singleton(DatabaseStorageInterface::class, DatabaseStorage::class);

        $this->app->singleton(CacheStorageInterface::class, function () {
            $config = config('file-storage.cache');

            return new RedisCache(
                $config['table'],
                $config['connection'],
            );
        });
    }
}
