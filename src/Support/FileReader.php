<?php

namespace Gsdk\FileStorage\Support;

use Gsdk\FileStorage\Contracts\CacheStorageInterface;
use Gsdk\FileStorage\Contracts\DatabaseStorageInterface;
use Gsdk\FileStorage\Entity\File;
use Gsdk\FileStorage\ValueObject\Guid;

class FileReader
{
    public function __construct(
        protected readonly DatabaseStorageInterface $databaseStorage,
        protected readonly CacheStorageInterface $cacheStorage,
    ) {}

    public function findByGuid(string $guid): ?File
    {
        $guid = new Guid($guid);
        $file = $this->cacheStorage->get($guid) ?? $this->databaseStorage->find($guid);
        if (null === $file) {
            return null;
        }

        $this->cacheStorage->store($file);

        return $file;
    }
}