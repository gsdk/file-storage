<?php

namespace Gsdk\FileStorage\Support;

use Gsdk\FileStorage\Contracts\CacheStorageInterface;
use Gsdk\FileStorage\Contracts\DatabaseStorageInterface;
use Gsdk\FileStorage\Contracts\FilesystemsStorageInterface;
use Gsdk\FileStorage\Entity\File;
use Gsdk\FileStorage\Exception\FileNotFoundException;
use Gsdk\FileStorage\ValueObject\Guid;

class FileUpdater
{
    public function __construct(
        protected readonly DatabaseStorageInterface $databaseStorage,
        protected readonly FilesystemsStorageInterface $filesystemsStorage,
        protected readonly CacheStorageInterface $cacheStorage,
    ) {}

    public function create(string $name, string $contents): File
    {
        $file = $this->databaseStorage->create($name);

        return $this->store($file, $contents);
    }

    public function update(string $guid, string $name, string $contents): ?File
    {
        $file = $this->databaseStorage->find(new Guid($guid));
        if (!$file) {
            throw new FileNotFoundException();
        }

        $this->filesystemsStorage->delete($file);

        $file->setName($name);
        $this->databaseStorage->store($file);

        return $this->store($file, $contents);
    }

    public function remove(string $guid): void
    {
        $file = $this->databaseStorage->find(new Guid($guid));
        if (null === $file) {
            throw new \Exception('');
        }

        $this->cacheStorage->forget($file);
        $this->filesystemsStorage->delete($file);
        $this->databaseStorage->delete($file);
    }

    private function store(File $file, string $contents): File
    {
        $this->filesystemsStorage->put($file, $contents);

        $this->cacheStorage->store($file);

        return $file;
    }
}