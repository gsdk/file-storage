<?php

namespace Gsdk\FileStorage\Service;

use Exception;
use Gsdk\FileStorage\Contracts\FilesystemsStorageInterface;
use Gsdk\FileStorage\Contracts\PathGeneratorInterface;
use Gsdk\FileStorage\Entity\File;

class FilesystemsStorage implements FilesystemsStorageInterface
{
    protected readonly array $config;

    public function __construct(
        array $config,
        protected readonly PathGeneratorInterface $pathGenerator
    ) {
        $this->config = [
            'dir_mode' => $config['dir_mode'] ?? 0770,
            'file_mode' => $config['file_mode'] ?? 0660,
            'user' => $config['user'] ?? null,
            'group' => $config['group'] ?? null,
        ];
    }

    public function get(File $file, int $part = null): ?string
    {
        $filename = $this->pathGenerator->path($file, $part);
        if (!file_exists($filename)) {
            return null;
        }

        return (string)file_get_contents($filename);
    }

    /**
     * @throws Exception
     */
    public function put(File $file, string $contents): bool
    {
        $filename = $this->pathGenerator->path($file);
        $createdFlag = !file_exists($filename);

        if ($createdFlag) {
            $umask = umask(0);
            static::checkFileDirectory(dirname($filename), $this->config['dir_mode']);
        }

        $lock = false;
        if (false === file_put_contents($filename, $contents, $lock ? LOCK_EX : 0)) {
            return false;
        }

        if ($createdFlag) {
            $this->chmod($filename, $umask);
        }

        return true;
    }

    public function delete(File $file): void
    {
        $filename = $this->pathGenerator->path($file);
        if (file_exists($filename)) {
            unlink($filename);
        }
    }

    private function chmod($filename, $umask): void
    {
        chmod($filename, $this->config['file_mode']);
        umask($umask);

        if ($this->config['group']) {
            chgrp($filename, $this->config['group']);
        }

        if ($this->config['user']) {
            chown($filename, $this->config['user']);
        }
    }

    private static function checkFileDirectory($path, $mode): void
    {
        if (is_dir($path)) {
            return;
        }

        if (!mkdir($path, $mode, true)) {
            throw new Exception('Cant create folder "' . $path . '"');
        }
    }
}
