<?php

namespace Gsdk\FileStorage\Service;

use Gsdk\FileStorage\Contracts\PathGeneratorInterface;
use Gsdk\FileStorage\Entity\File;

class PathGenerator implements PathGeneratorInterface
{
    protected readonly string $rootPath;

    public function __construct(
        string $rootPath,
        protected readonly int $nestingLevel,
        protected readonly int $pathNameLength,
    ) {
        $this->rootPath = rtrim($rootPath, '/');
    }

    public function relativePath(File $file): string
    {
        return implode(DIRECTORY_SEPARATOR, $this->guidPaths($file->guid()->value()))
            . DIRECTORY_SEPARATOR . $file->guid()->value();
    }

    public function path(File $file, int $part = null): string
    {
        return $this->rootPath
            . DIRECTORY_SEPARATOR . $this->relativePath($file)
            . DIRECTORY_SEPARATOR . $file->name();
    }

    private function guidPaths(string $guid): array
    {
        $paths = [];
        for ($i = 0; $i < $this->nestingLevel; $i++) {
            $paths[] = substr($guid, $i * $this->pathNameLength, $this->pathNameLength);
        }

        return $paths;
    }
}
