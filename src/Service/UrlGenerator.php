<?php

namespace Gsdk\FileStorage\Service;

use Gsdk\FileStorage\Contracts\PathGeneratorInterface;
use Gsdk\FileStorage\Contracts\UrlGeneratorInterface;
use Gsdk\FileStorage\Entity\File;
use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Support\Facades\Storage;

class UrlGenerator implements UrlGeneratorInterface
{
    protected FilesystemAdapter $storage;

    public function __construct(
        string $disk,
        protected readonly PathGeneratorInterface $pathGenerator
    ) {
        $this->storage = Storage::disk($disk);
    }

    public function url(File $file, int $part = null): ?string
    {
        return $this->storage->url(
            str_replace(DIRECTORY_SEPARATOR, '/', $this->pathGenerator->relativePath($file))
            . '/' . rawurlencode($file->name())
        );
    }
}
