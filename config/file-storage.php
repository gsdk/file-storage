<?php

$mode = function (string $key, int $default): int {
    $stringValue = env($key);

    return $stringValue ? octdec($stringValue) : $default;
};

return [
    'filesystems' => [
        'disk' => env('FILE_STORAGE_DISK', 'files'),
        'nesting_level' => env('FILE_STORAGE_NESTING_LEVELS', 2),
        'path_name_length' => env('FILE_STORAGE_PATH_LENGTH', 2),
        'dir_mode' => $mode('FILE_STORAGE_DIR_MODE', 0770),
        'file_mode' => $mode('FILE_STORAGE_FILE_MODE', 0660),
        'user' => env('FILE_STORAGE_SYSTEM_USER'),
        'group' => env('FILE_STORAGE_SYSTEM_GROUP'),
    ],
    'database' => [
        'table' => env('FILE_STORAGE_DB_TABLE', 'files'),
    ],
    'cache' => [
        'driver' => 'redis',
        'connection' => 'cache',
        'table' => 'files'
    ]
];